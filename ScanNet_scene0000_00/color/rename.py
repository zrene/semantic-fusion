import os

ending = ".txt"

files = [f.replace(ending, "")  for f in os.listdir() if ending in f]
files.sort(key=int)
i = 0
for i in range(len(files)):
    newName = "{:05d}".format(i) + ".txt"
    print("renaming ", files[i] + ending, " -> ", newName)
    os.rename(files[i] + ending, newName)
    i += 1
