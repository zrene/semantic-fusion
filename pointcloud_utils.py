# -*- coding: UTF-8 -*-
import pclpy
from pclpy import pcl

import numpy as np
import open3d as o3d


def Pcl2O3d(pc):
    """ Cpmverts a PCL point cloud to an open3d point cloud"""
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(pc.xyz)
    pcd.colors = o3d.utility.Vector3dVector(pc.rgb)

    return pcd


def storeTriangleMesh(pc, normals, path):

    pcd = o3d.geometry.PointCloud()
    print(pc)
    print(pc[:,3:6])

    pcd.points = o3d.utility.Vector3dVector(pc[:, 0:3])
    pcd.colors = o3d.utility.Vector3dVector(pc[:, 3:6]/255)
    pcd.normals = o3d.utility.Vector3dVector(o3d.utility.Vector3dVector(normals))

    # estimate radius for rolling ball
    distances = pcd.compute_nearest_neighbor_distance()
    avg_dist = np.mean(distances)
    radius = 1.5 * avg_dist
    mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(pcd, o3d.utility.DoubleVector([radius, radius * 2]))
    o3d.io.write_triangle_mesh(path, mesh)