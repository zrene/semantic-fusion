import multiprocessing as mp
import multiprocessing as mp
import threading
import time
from queue import Empty as queue_empty

import numpy as np
import open3d as o3d
import pclpy
from pclpy import pcl

import SemanticFuser


use_sopervoxel_postproc = True
n_images = 30

if __name__ == '__main__':
    fuser = SemanticFuser.SemanticFuser(n_imgs=n_images)

    for i in range(n_images-1):
        fuser.fuse_images(i)

    fuser.fuse_images(n_images)
    fuser.prepareVoxelExtraction()
    fuser.extractPc()
    fusion_seg, _ = fuser.svox.extract_point_cloud()


    if use_sopervoxel_postproc:
        fuser.prepareVoxelExtraction()
        fuser.svox.extract_point_cloud()
        fuser.createSuperVoxels()
        svox_seg_pc = fuser.svox.pc_segmented

        viewer = pcl.visualization.PCLVisualizer("(using Supervoxel) Viewer")
        viewer.setPosition(0, 0)
        viewer.setSize(800, 400)
        viewer.addPointCloud(svox_seg_pc)


    segmentationViewer = pcl.visualization.PCLVisualizer("(no supervoxel) Viewer")
    segmentationViewer.setPosition(0, 500)
    segmentationViewer.setSize(800, 400)
    segmentationViewer.addPointCloud(fusion_seg)

    while True and not segmentationViewer.wasStopped():
        if use_sopervoxel_postproc:
            viewer.spinOnce(100)
        segmentationViewer.spinOnce(100)
        time.sleep(0.1)




