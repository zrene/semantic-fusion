# from random import randint
# def get_40_classes(path="classNames40.txt"):
#     f = open(path, "r")
#     l = f.readline()
#     items = l[:-2].split(",")
#     i = 1
#     dict = {}
#     for item in items:
#         dict[i] = item
#         i+=1
#
#     print(dict)
#     print(dict.__len__())
#     return dict
#
# import csv
# with open("labels_40.csv", "w", newline='') as file:
#     f = csv.writer(file, delimiter= ';')
#     items = get_40_classes()
#     i = 1
#     f.writerow([0, "unknown", 0,0,0])
#     for key in items.keys():
#         f.writerow([key,items[key], randint(0,255), randint(0,255), randint(0,255)])
#
# print("done")

import preprocess_masks_v2 as pre
import csv

def get_classes_to_extract(classes):
    dict = {}
    for key in classes.keys():
        dict[classes[key]] = int(key)
    return dict

#For legacy support, so we can still use the old nyu data
def nyu_classes(path= "labels_old.csv"):
    with open(path, newline='') as csvfile:
        labelreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        labels = {}
        for row in labelreader:
            labels[row[0]] = row[1]
        print(labels)
    mapping = {
        'Unknown': 0,
        'Bed': 1,
        'Books': 2,
        'Ceiling': 3,
        "Chair": 4,
        "Floor": 5,
        "Furniture": 6,
        "Object": 7,
        "Picture": 8,
        "Sofa": 9,
        "Table": 10,
        "TV": 11,
        "Wall": 12,
        "Window": 13,
    }
    classes_to_extract = {
        'Computer': 66,
        'Paper': 15,
        'Towel': 135,
        'Bottle': 2,
        "Wall" : 21,
        "Pillow": 119,
        "Desk" :  36,
    }
    color_dict = {
        66: (255, 0, 0),
        15: (0, 0, 255),
        135: (255, 0, 0),
        2: (255, 255, 0),
        21: (255, 255, 255),
        119: (255, 0, 255),
        36: (0, 255, 255)
    }
    return labels, mapping, color_dict, classes_to_extract



labels_path = "labels_40.csv"
classes, mapping, color_dict = pre.get_classes(labels_path)
classes_to_extract = get_classes_to_extract(classes)
print("first")
print(classes)
print(mapping)
print(color_dict)
print(classes_to_extract)

labels_path = "labels.csv"
classes, mapping, color_dict = pre.get_classes(labels_path)
classes_to_extract = get_classes_to_extract(classes)
print("first")
print(classes)
print(mapping)
print(color_dict)
print(classes_to_extract)

labels_path = "labels_old.csv"
classes, mapping, color_dict, classes_to_extract = nyu_classes(labels_path)
print("first")
print(classes)
print(mapping)
print(color_dict)
print(classes_to_extract)

