
# examples/Python/Advanced/rgbd_integration.py

import open3d as o3d
import numpy as np
import numpy.linalg
import time
import os

class CameraPose:
    def __init__(self, meta, mat):
        self.metadata = meta
        self.pose = mat

    def __str__(self):
        return 'Metadata : ' + ' '.join(map(str, self.metadata)) + '\n' + \
               "Pose : " + "\n" + np.array_str(self.pose)


def read_trajectory(folder):
    traj = []
    for filename in [f for f in os.listdir("data") if (".txt" in f and not "intrinsics.txt" in f)]:
        print(filename)
        with open(folder + "/" + filename, 'r') as f:
            metastr = f.readline();
            print(metastr)
            print(metastr.split())
            while metastr:
                #metadata = map(int, metastr.split())
                metadata = filename
                mat = np.zeros(shape=(4, 4))

                for i in range(4):
                    mat[i, :] = np.fromstring(metastr, dtype=float, sep=' \t')
                    metastr = f.readline();
                traj.append(CameraPose(metadata, mat))
                #metastr = f.readline()
    return traj

if __name__ == '__main__':
    #print([ p.pose for p in read_trajectory("data")])

    camera_poses = read_trajectory("data")

    volume = o3d.integration.ScalableTSDFVolume(
        voxel_length=4.0 / 512.0,
        sdf_trunc=0.04,
        color_type=o3d.integration.TSDFVolumeColorType.RGB8)
    t1 = time.time()
    for i in range(min(len(camera_poses), 90)):
        i_start = 1651
        print("Integrate {:d}-th image into the volume.".format(i))

        #color = o3d.io.read_image(
        #    "data/{:05d}.jpg".format(i+i_start))
        color = o3d.io.read_image("mask_data/{:05d}/color_image.jpg".format(i+i_start))
        depth = o3d.io.read_image(
            "data/{:05d}.png".format(i+i_start))
        rgbd = o3d.geometry.RGBDImage.create_from_color_and_depth(
            color, depth, depth_trunc=4.0, convert_rgb_to_intensity=False)
        volume.integrate(
            rgbd,
            o3d.camera.PinholeCameraIntrinsic(
                o3d.camera.PinholeCameraIntrinsicParameters.PrimeSenseDefault),
            np.linalg.inv(camera_poses[i].pose))

    diff =  time.time() - t1
    print("frames per sec", len(camera_poses)/diff)

    print("Extract a triangle mesh from the volume and visualize it.")
    t1 = time.time()
    mesh = volume.extract_point_cloud()

    #mesh.compute_vertex_normals()
    print("took", time.time()-t1)
    o3d.visualization.draw_geometries([mesh])

    #print([f for f in os.listdir("data") if ".txt" in f])