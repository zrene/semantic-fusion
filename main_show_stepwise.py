import multiprocessing as mp
import multiprocessing as mp
import threading
import time
from queue import Empty as queue_empty

import numpy as np
import open3d as o3d
import pclpy
from pclpy import pcl

import SemanticFuser
import pointcloud_utils as pc_utils

images = 10
extract_supervoxel = False

fuser = SemanticFuser.SemanticFuser(n_imgs = images)

pc = None
pc_seg = None
pc_not_vox = None
pc_orig = None

def worker():
   """
   Background thread extracting supervoxels, since Visualizer must be running in the main thread.
   Cant both be in the main thread, otherwise visualizer freezes
   :return:
   """

   global pc
   global pc_seg
   global pc_not_vox
   global pc_orig
   global extract_supervoxel

   pc_not_vox, pc_orig = fuser.svox.extract_point_cloud()

   if extract_supervoxel:
        pc, pc_seg = fuser.createSuperVoxels()

   print("----------> done ")

if __name__ == '__main__':
    if extract_supervoxel:
        viewer = pcl.visualization.PCLVisualizer("SuperVoxel Viewer")
        viewer.setPosition(0,0)
        viewer.setSize(800,400)

        segmentationViewer = pcl.visualization.PCLVisualizer("SuperVoxel Segmentation Viewer")
        segmentationViewer.setPosition(810,0)
        segmentationViewer.setSize(800,400)

    fusionViewer = pcl.visualization.PCLVisualizer("Fusion Viewer")
    fusionViewer.setPosition(0,500)
    fusionViewer.setSize(800,400)

    origViewer = pcl.visualization.PCLVisualizer("Original Viewer")
    origViewer.setPosition(810, 500)
    origViewer.setSize(800, 400)

    keep_running = True
    started = False
    started_2 = False

    for i in range(images):
        fuser.fuse_images(i)
        fuser.prepareVoxelExtraction()
        pc_fusion = fuser.extractPc()

        p = threading.Thread(target=worker)
        p.start()

        while (pc_not_vox is None or (extract_supervoxel and pc is None)) and keep_running:

            if started:
                if extract_supervoxel:
                    viewer.spinOnce(50)
                    segmentationViewer.spinOnce(50)

                fusionViewer.spinOnce(50)
                origViewer.spinOnce(50)

            time.sleep(0.1)

        if started:

            if extract_supervoxel:
                viewer.removePointCloud('cloud')
                segmentationViewer.removePointCloud('cloud')

            fusionViewer.removePointCloud('cloud1')
            origViewer.removePointCloud('cloud1')



        print("addinc pc")

        print(pc_not_vox, pc_orig, pc, pc_seg)
        if extract_supervoxel:
            viewer.addPointCloud(pc, 'cloud')
            segmentationViewer.addPointCloud(pc_seg, 'cloud')

        fusionViewer.addPointCloud(pc_not_vox, 'cloud1')
        origViewer.addPointCloud(pc_orig, 'cloud1')

        if not started:
            pos = np.mean(pc_not_vox.xyz, axis=0)
            p = pclpy.pcl.vectors.Camera()
            fusionViewer.getCameras(p)
            v = p.pop().view()

            if extract_supervoxel:
                viewer.setCameraPosition(pos[0], pos[1], pos[2], v[0], v[1], v[2])
                segmentationViewer.setCameraPosition(pos[0], pos[1], pos[2], v[0], v[1], v[2])

            fusionViewer.setCameraPosition(pos[0], pos[1], pos[2], v[0], v[1], v[2])
            origViewer.setCameraPosition(pos[0], pos[1], pos[2], v[0], v[1], v[2])

        if extract_supervoxel:
            viewer.spinOnce(100)
            segmentationViewer.spinOnce(50)

        fusionViewer.spinOnce(50)
        origViewer.spinOnce(50)

        started = True
        pc_not_vox = None

    while True:
        if extract_supervoxel:
            viewer.spinOnce(100)
            segmentationViewer.spinOnce(50)

        fusionViewer.spinOnce(50)
        origViewer.spinOnce(50)


