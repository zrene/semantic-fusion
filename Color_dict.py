#Color dictionaries and some stuff for the new dataset

color_dict = {
        0: (255, 0, 0),
        1: (0, 255, 0),
        2: (0, 0, 255),
        3: (255, 0, 255),
        4: (0, 255, 255),
        5: (139,69,19),
        6: (139,69,19),
        7: (255, 255, 0),
        9: (255, 20,147),
        10: (147, 0,147),
        11: (147, 147,0),
        12: (147, 147,147),
        13: (255, 255,255)
    }
classes_to_extract = {
        'Unknown': 0,
        'Bed': 1,
        'Books': 2,
        'Ceiling': 3,
        "Chair": 4,
        "Floor": 5,
        "Furniture": 6,
        "Object": 7,
        "Picture": 8,
        "Sofa": 9,
        "Table": 10,
        "TV": 11,
        "Wall": 12,
        "Window": 13,
    }

classes =  {
        "0":'Unknown',
        "1":'Bed',
        "2":'Books',
        "3":'Ceiling',
        "4":'Chair',
        "5":'Floor',
        "6":'Furniture',
        "7": 'Object',
        "8": 'Picture',
        "9":'Sofa',
        "10":'Table',
        "11":'TV',
        "12":'Wall',
        "13":'Window'
         }