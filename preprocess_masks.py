from PIL import Image, ImageDraw

import os
import cv2
import csv
import numpy as np

import re


col = {
    'pillow':  	(255,0,0,255),
    'picture': (255,215,0,255),
    'bed': (255,0,0,255),
    'book': (255,255,255,255),
    'paper': (0,255,0,255),
    'printer': (255,0,0,255),
    'cup': (255,0,255,255),
    'monitor': (0,0,255,255)
}

def getColorForLabel(label):
    """ Returns a color for a class label """
    return col.get(label, (255,0,0,255))


# Restrict ourself to this classes

allowed_classes = [66, 15, 135, 2, 119, 36, 19, 21,1,119]

pattern = re.compile("\d+_\d+_.+")

def load_result(path):
    """ Loads masks, labels and scores from the output folder"""
    masks = []
    labels = []
    scores = []

    for name in os.listdir(path):
        if pattern.match(name):
            mask = Image.open(path + "/" + name)
            n = name.split("_")

            if len(n) < 3:
                continue

            label = int(n[1])
            score = float(n[2][:-4])

            masks.append(mask)
            labels.append(label)
            scores.append(score)

    # order everything be score. lowest score first
    index_scores = np.array([np.arange(len(masks)), scores]).T
    index_scores = index_scores[index_scores[:, 1].argsort()]

    masks_sorted = []
    labels_sorted = []
    scores_sorted = []

    for ind in index_scores[:,0]:
        ind = int(ind)

        masks_sorted.append(masks[ind])
        labels_sorted.append(labels[ind])
        scores_sorted.append(scores[ind])


    return masks_sorted, labels_sorted, scores_sorted


def get_classes(path):
    """ Gets mapping class number <-> class label"""

    with open(path, newline='') as csvfile:
        labelreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        labels = {}
        for row in labelreader:
            labels[row[0]] = row[1]
    return labels


def preprocess_all(mask_path, data_path):
    """ Converts mask images into class_score image and creates a color image using the masks.
        Class_score image: Just a regular RGB image, containing the class number as G, and the Score as B value
        Color image: RGB image, input image with masks as overlay. Masks are colored according to their class """

    for file in os.listdir(mask_path):
        print("Going to convert masks from file: " + file)

        print("Creating image containing class and score information")
        create_class_image(mask_path + "/" + file, mask_path + "/" + file + "/class_image.png", Image.open(data_path + "/" + file + ".jpg").convert("RGBA"), classes = get_classes('labels.csv'), allowed_classes = allowed_classes)

        print("Creating color model")
        create_image_with_class_color(mask_path + "/" + file, mask_path + "/" + file + "/color_image.jpg", Image.open(data_path + "/" + file + ".jpg").convert("RGBA"), classes= get_classes('labels.csv'), allowed_classes = allowed_classes)


def create_class_image(path, output_path, input, classes, allowed_classes):
    """ Creates a class image: Just a regular RGB image, containing the class number as G, and the Score as B value"""
    masks, labels, scores = load_result(path)

    # Create a new image to draw on
    box_image = Image.new('RGB', input.size, (0, 0, 0))

    # Convert to numpy array, since using Pillow created artifacts while filling bitmap
    img = np.asarray(box_image).copy().astype(int)

    # All pixels in the mask with a value bigger than this threshold are
    # assigned to the binary mask.
    mask_threshold = 0.5

    # All detections with a score bigger than this threshold are used as actual
    # detections.
    score_threshold = 0.5

    # Extract predicted boxes and masks.
    for i in range(len(masks)):

        mask = masks[i]
        score = scores[i]
        label = labels[i]
        score = scores[i]

        # Check if we want to create an instance from this class
        class_name = classes[str(label)]
        if (score < score_threshold) or (int(label) not in allowed_classes):
            print(class_name, "(blocked) number: ", label)
            continue

        print(class_name)
        print("got: ", label)
        print("score ", score)

        # Convert score to a value [0,256]
        value = int(score * 256)
        print("value: ", value)

        # This does not work, since pillow somehow interpolates or sth and values will get smeared out
        # box_image_draw.bitmap((0, 0), mask, fill=fill)

        # Only keep values of masks that are higher than this threshold
        # (problem with jpg. compression creates artifacts)
        # which lead to the image not being pure binary
        threshold = 200
        mask_arr = np.asarray(mask)


        # Set G channel of this mask to the value of the class

        img[(mask_arr > threshold), 1] = label
        # Set B channel of this mask to the score of the prediction
        img[(mask_arr > threshold) , 0] = int(score * 255)

    cv2.imwrite(output_path, img)


def create_image_with_class_color(path, output_path, input, classes, allowed_classes, score_threshold = 0.5):
    """Color image: RGB image, input image with masks as overlay. Masks are colored according to their class """
    masks, labels, scores = load_result(path)

    box_image = Image.new('RGBA', input.size, (255, 255, 255, 0))
    box_image_draw = ImageDraw.Draw(box_image)

    # Extract predicted boxes and masks.
    for i in range(len(masks)):
        mask = masks[i]
        label = labels[i]
        score = scores[i]

        if (score < score_threshold) or (int(label) not in allowed_classes):
            continue

        label = classes[str(label)]
        box_image_draw.bitmap((0, 0), mask, fill=getColorForLabel(label))

    # Save the predicted test image.
    out = Image.alpha_composite(input, box_image).convert("RGB")
    out.save(output_path, "JPEG")

