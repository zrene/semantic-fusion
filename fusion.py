# Copyright (c) 2018 Andy Zeng
import csv

import numpy as np
from numba import njit, prange
from skimage import measure
import warnings
import time
import colorsys
from numpy import ravel
import trimesh
usePC = True
from scipy import ndimage
from scipy.stats import mode
from skimage import morphology

try:
    import pcl
    import pcl.pcl_visualization
except Exception as err:
    usePC = False
    print("point cloud library not installed")

try:
    import pycuda.driver as cuda
    import pycuda.autoinit
    from pycuda.compiler import SourceModule

    FUSION_GPU_MODE = 1
except Exception as err:
    print('Warning: {}'.format(err))
    print('Failed to import PyCUDA. Running fusion in CPU mode.')
    FUSION_GPU_MODE = 0


# def load_labels(path):
#     with open(path, newline='') as csvfile:
#         labelreader = csv.reader(csvfile, delimiter=';', quotechar='|')
#         l = {}
#         for row in labelreader:
#             l[row[0]] = row[1]

# Print debug informations
debug = False
debug_classes = True
checkDuplicate = True

# load_labels("labels.csv")
# print(labels)

# with open('labels_40.csv', newline='') as csvfile:
#     labelreader = csv.reader(csvfile, delimiter=';', quotechar='|')
#     labels = {}
#     for row in labelreader:
#         labels[row[0]] = row[1]



def hsv_to_rgb(h, s, v):
    """ helper functions used to color point cloud usgin pcl. """
    shape = h.shape
    i = np.int_(h * 6.)
    f = h * 6. - i

    q = f
    t = 1. - f
    i = np.ravel(i)
    f = np.ravel(f)
    i %= 6

    t = np.ravel(t)
    q = np.ravel(q)

    clist = (1 - s * np.vstack([np.zeros_like(f), np.ones_like(f), q, t])) * v

    # 0:v 1:p 2:q 3:t
    order = np.array([[0, 3, 1], [2, 0, 1], [1, 0, 3], [1, 2, 0], [3, 1, 0], [0, 1, 2]])
    rgb = clist[order[i], np.arange(np.prod(shape))[:, None]]

    return rgb.reshape(shape + (3,))



class TSDFVolume:
    """Volumetric TSDF Fusion of RGB-D Images.
  """

    def setValues(self, _tsdf_vol_cpu,_color_vol_cpu,_class_vol_cpu):

        self._tsdf_vol_cpu =_tsdf_vol_cpu
        self._color_vol_cpu = _color_vol_cpu
        self._class_vol_cpu = _class_vol_cpu

        self._tsdf_vol_gpu = _tsdf_vol_cpu
        self._color_vol_cpu = _color_vol_cpu
        self._class_vol_cpu = _class_vol_cpu



    def __init__(self, vol_bnds, voxel_size, filter_interval = -1, use_gpu=True):
        """Constructor.

    Args:
      vol_bnds (ndarray): An ndarray of shape (3, 2). Specifies the
        xyz bounds (min/max) in meters.
      voxel_size (float): The volume discretization in meters.
    """
        vol_bnds = np.asarray(vol_bnds)
        assert vol_bnds.shape == (3, 2), "[!] `vol_bnds` should be of shape (3, 2)."

        if(usePC):
            self.visual = pcl.pcl_visualization.CloudViewing()
        self.filter_interval = filter_interval


        self.frame = 0
        # Define voxel volume parameters
        self._vol_bnds = vol_bnds
        self._voxel_size = float(voxel_size)
        self._trunc_margin = 5 * self._voxel_size  # truncation on SDF
        self._color_const = 256 * 256
        self._class_const = 256 * 256 * 256

        # Adjust volume bounds and ensure C-order contiguous
        self._vol_dim = np.ceil((self._vol_bnds[:, 1] - self._vol_bnds[:, 0]) / self._voxel_size).copy(
            order='C').astype(int)
        self._vol_bnds[:, 1] = self._vol_bnds[:, 0] + self._vol_dim * self._voxel_size
        self._vol_origin = self._vol_bnds[:, 0].copy(order='C').astype(np.float32)

        print("Voxel volume size: {} x {} x {} - # points: {:,}".format(
            self._vol_dim[0], self._vol_dim[1], self._vol_dim[2],
            self._vol_dim[0] * self._vol_dim[1] * self._vol_dim[2])
        )

        # Initialize pointers to voxel volume in CPU memory
        self._tsdf_vol_cpu = np.ones(self._vol_dim).astype(np.float32)
        # for computing the cumulative moving average of observations per voxel
        self._weight_vol_cpu = np.zeros(self._vol_dim).astype(np.float32)
        self._color_vol_cpu = np.zeros(self._vol_dim).astype(np.float32)

        self._class_vol_cpu = np.zeros(self._vol_dim).astype(np.float32)
        # self._score_vol_cpu = np.zeros(self._vol_dim).astype(np.float32)

        self.gpu_mode = use_gpu and FUSION_GPU_MODE

        # Copy voxel volumes to GPU
        if self.gpu_mode:
            self._tsdf_vol_gpu = cuda.mem_alloc(self._tsdf_vol_cpu.nbytes)
            cuda.memcpy_htod(self._tsdf_vol_gpu, self._tsdf_vol_cpu)

            self._weight_vol_gpu = cuda.mem_alloc(self._weight_vol_cpu.nbytes)
            cuda.memcpy_htod(self._weight_vol_gpu, self._weight_vol_cpu)

            self._color_vol_gpu = cuda.mem_alloc(self._color_vol_cpu.nbytes)
            cuda.memcpy_htod(self._color_vol_gpu, self._color_vol_cpu)

            # Class information for each voxel
            self._class_vol_gpu = cuda.mem_alloc(self._class_vol_cpu.nbytes)
            cuda.memcpy_htod(self._class_vol_gpu, self._class_vol_cpu)

            """# Score information for each voxel
      self._score_vol_gpu = cuda.mem_alloc(self._score_vol_cpu.nbytes)
      cuda.memcpy_htod(self._score_vol_gpu, self._score_vol_cpu) """

            # Cuda kernel function (C++)
            self._cuda_src_mod = SourceModule("""
       __global__ void integrate(float * tsdf_vol,
                                  float * weight_vol,
                                  float * color_vol,
                                  float * class_vol,
                                  float * vol_dim,
                                  float * vol_origin,
                                  float * cam_intr,
                                  float * cam_pose,
                                  float * other_params,
                                  float * color_im,
                                  float * depth_im,
                                  float * class_im) {
          // Get voxel index
          int gpu_loop_idx = (int) other_params[0];
          int max_threads_per_block = blockDim.x;
          int block_idx = blockIdx.z*gridDim.y*gridDim.x+blockIdx.y*gridDim.x+blockIdx.x;
          int voxel_idx = gpu_loop_idx*gridDim.x*gridDim.y*gridDim.z*max_threads_per_block+block_idx*max_threads_per_block+threadIdx.x;
          int vol_dim_x = (int) vol_dim[0];
          int vol_dim_y = (int) vol_dim[1];
          int vol_dim_z = (int) vol_dim[2];
          if (voxel_idx > vol_dim_x*vol_dim_y*vol_dim_z)
              return;
              
              
          // Get voxel grid coordinates (note: be careful when casting)
          float voxel_x = floorf(((float)voxel_idx)/((float)(vol_dim_y*vol_dim_z)));
          float voxel_y = floorf(((float)(voxel_idx-((int)voxel_x)*vol_dim_y*vol_dim_z))/((float)vol_dim_z));
          float voxel_z = (float)(voxel_idx-((int)voxel_x)*vol_dim_y*vol_dim_z-((int)voxel_y)*vol_dim_z);
          
          // Voxel grid coordinates to world coordinates
          float voxel_size = other_params[1];
          float pt_x = vol_origin[0]+voxel_x*voxel_size;
          float pt_y = vol_origin[1]+voxel_y*voxel_size;
          float pt_z = vol_origin[2]+voxel_z*voxel_size;
          
          // World coordinates to camera coordinates
          float tmp_pt_x = pt_x-cam_pose[0*4+3];
          float tmp_pt_y = pt_y-cam_pose[1*4+3];
          float tmp_pt_z = pt_z-cam_pose[2*4+3];
          float cam_pt_x = cam_pose[0*4+0]*tmp_pt_x+cam_pose[1*4+0]*tmp_pt_y+cam_pose[2*4+0]*tmp_pt_z;
          float cam_pt_y = cam_pose[0*4+1]*tmp_pt_x+cam_pose[1*4+1]*tmp_pt_y+cam_pose[2*4+1]*tmp_pt_z;
          float cam_pt_z = cam_pose[0*4+2]*tmp_pt_x+cam_pose[1*4+2]*tmp_pt_y+cam_pose[2*4+2]*tmp_pt_z;
          
          // Camera coordinates to image pixels
          int pixel_x = (int) roundf(cam_intr[0*3+0]*(cam_pt_x/cam_pt_z)+cam_intr[0*3+2]);
          int pixel_y = (int) roundf(cam_intr[1*3+1]*(cam_pt_y/cam_pt_z)+cam_intr[1*3+2]);
          
          // Skip if outside view frustum
          int im_h = (int) other_params[2];
          int im_w = (int) other_params[3];
          if (pixel_x < 0 || pixel_x >= im_w || pixel_y < 0 || pixel_y >= im_h || cam_pt_z<0)
              return;

          // Skip invalid depth
          float depth_value = depth_im[pixel_y*im_w+pixel_x];
          if (depth_value == 0)
              return;

          // Integrate TSDF
          float trunc_margin = other_params[4];
          float depth_diff = depth_value-cam_pt_z;
          if (depth_diff < -trunc_margin)
              return;

          float dist = fmin(1.0f,depth_diff/trunc_margin);
          float w_old = weight_vol[voxel_idx];
          float obs_weight = other_params[5];
          float w_new = w_old + obs_weight;
          weight_vol[voxel_idx] = w_new;
          tsdf_vol[voxel_idx] = (tsdf_vol[voxel_idx]*w_old+obs_weight*dist)/w_new;
          
          
          // Integrate color
          float old_color = color_vol[voxel_idx];
          float old_b = floorf(old_color/(256*256));
          float old_g = floorf((old_color-old_b*256*256)/256);
          float old_r = old_color-old_b*256*256-old_g*256;
          
          float new_color = color_im[pixel_y*im_w+pixel_x];
          float new_b = floorf(new_color/(256*256));
          float new_g = floorf((new_color-new_b*256*256)/256);
          float new_r = new_color-new_b*256*256-new_g*256;
          
          
          new_b = fmin(roundf((old_b*w_old+obs_weight*new_b)/w_new),255.0f);
          new_g = fmin(roundf((old_g*w_old+obs_weight*new_g)/w_new),255.0f);
          new_r = fmin(roundf((old_r*w_old+obs_weight*new_r)/w_new),255.0f);
          color_vol[voxel_idx] = new_b*256*256+new_g*256+new_r;
          


           // Integrate classes
           float old_class = class_vol[voxel_idx];
           float old_score = floorf(old_class/(256*256));
          
           float new_class = class_im[pixel_y*im_w+pixel_x];
           float new_score = floorf(new_class/(256*256));
           
           float a = 1.0;
           float b = 0.0;//4.0;
           float c = 0.0;//0.1;
           
           // method 1)
           new_score = a * new_score + b* 1.0f/depth_value/depth_value + c * obs_weight/w_new;
           new_score = floorf(fmax(fmin(new_score,255.0f),0.0f));
           old_score = a * old_score + b* 1.0f/depth_value/depth_value + c * obs_weight/w_old;
           old_score = floorf(fmax(fmin(old_score,255.0f),0.0f));
           
           // method 2)
           //new_score = a * 1.0f/depth_value * obs_weight/w_new * new_score;
           //new_score = floorf(fmax(fmin(new_score,255.0f),0.0f));
           
           float old_label = floorf((old_class-old_score*256*256)/256);
           float new_label = floorf((new_class-new_score*256*256)/256);

            if(new_label == 0.0f) 
                return;
                
            if(old_label == 0.0f || old_score <= 0.1f) {
            
            } else if(old_label == new_label) {
                new_score = fmin((old_score + new_score),255.0f);
            } else {
                new_score = fmax((old_score - new_score), 0.0f);
            }
              
            class_vol[voxel_idx] = new_score*256*256+new_label*256;
            return;
        }""")

            self._cuda_integrate = self._cuda_src_mod.get_function("integrate")

            # Determine block/grid size on GPU
            gpu_dev = cuda.Device(0)
            self._max_gpu_threads_per_block = gpu_dev.MAX_THREADS_PER_BLOCK
            n_blocks = int(np.ceil(float(np.prod(self._vol_dim)) / float(self._max_gpu_threads_per_block)))
            grid_dim_x = min(gpu_dev.MAX_GRID_DIM_X, int(np.floor(np.cbrt(n_blocks))))
            grid_dim_y = min(gpu_dev.MAX_GRID_DIM_Y, int(np.floor(np.sqrt(n_blocks / grid_dim_x))))
            grid_dim_z = min(gpu_dev.MAX_GRID_DIM_Z, int(np.ceil(float(n_blocks) / float(grid_dim_x * grid_dim_y))))
            self._max_gpu_grid_dim = np.array([grid_dim_x, grid_dim_y, grid_dim_z]).astype(int)
            self._n_gpu_loops = int(np.ceil(float(np.prod(self._vol_dim)) / float(
                np.prod(self._max_gpu_grid_dim) * self._max_gpu_threads_per_block)))

        else:
            # Get voxel grid coordinates
            xv, yv, zv = np.meshgrid(
                range(self._vol_dim[0]),
                range(self._vol_dim[1]),
                range(self._vol_dim[2]),
                indexing='ij'
            )
            self.vox_coords = np.concatenate([
                xv.reshape(1, -1),
                yv.reshape(1, -1),
                zv.reshape(1, -1)
            ], axis=0).astype(int).T


    def filterClasses(self, class_vol, radius = 2):
        print("filter class volume")
        scores = np.floor(class_vol /self._color_const)
        class_numbers = np.floor((class_vol - scores * self._color_const) / 256)
        # class_numbers = ndimage.filters.generic_filter(class_numbers, filter, footprint=morphology.ball(4))
        class_numbers = ndimage.median_filter(class_numbers, footprint=morphology.ball(radius))
        print("numbers", np.unique(class_numbers))

        return class_numbers*256 + scores*self._color_const

    @staticmethod
    @njit(parallel=True)
    def vox2world(vol_origin, vox_coords, vox_size):
        """Convert voxel grid coordinates to world coordinates.
    """
        vol_origin = vol_origin.astype(np.float32)
        vox_coords = vox_coords.astype(np.float32)
        cam_pts = np.empty_like(vox_coords, dtype=np.float32)
        for i in prange(vox_coords.shape[0]):
            for j in range(3):
                cam_pts[i, j] = vol_origin[j] + (vox_size * vox_coords[i, j])
        return cam_pts

    @staticmethod
    @njit(parallel=True)
    def cam2pix(cam_pts, intr):
        """Convert camera coordinates to pixel coordinates.
    """
        intr = intr.astype(np.float32)
        fx, fy = intr[0, 0], intr[1, 1]
        cx, cy = intr[0, 2], intr[1, 2]
        pix = np.empty((cam_pts.shape[0], 2), dtype=np.int64)
        for i in prange(cam_pts.shape[0]):
            pix[i, 0] = int(np.round((cam_pts[i, 0] * fx / cam_pts[i, 2]) + cx))
            pix[i, 1] = int(np.round((cam_pts[i, 1] * fy / cam_pts[i, 2]) + cy))
        return pix


    def filterVolume(self):
        cuda.memcpy_dtoh(self._class_vol_cpu, self._class_vol_gpu)
        self._class_vol_cpu = self.filterClasses(self._class_vol_cpu, 3).astype(np.float32)
        cuda.memcpy_htod(self._class_vol_gpu,  self._class_vol_cpu)

    @staticmethod
    @njit(parallel=True)
    def integrate_tsdf(tsdf_vol, dist, w_old, obs_weight):
        """Integrate the TSDF volume.
    """
        tsdf_vol_int = np.empty_like(tsdf_vol, dtype=np.float32)
        w_new = np.empty_like(w_old, dtype=np.float32)
        for i in prange(len(tsdf_vol)):
            w_new[i] = w_old[i] + obs_weight
            tsdf_vol_int[i] = (w_old[i] * tsdf_vol[i] + obs_weight * dist[i]) / w_new[i]
        return tsdf_vol_int, w_new

    def integrate(self, color_im, depth_im, class_im, cam_intr, cam_pose, labels, obs_weight=1.):
        """Integrate an RGB-D frame into the TSDF volume.
    Args:
      color_im (ndarray): An RGB image of shape (H, W, 3).
      depth_im (ndarray): A depth image of shape (H, W).
      class_im (ndarray): A "RGB" image of shape (H,W,3). Containing the class number as B and the score as G value
      cam_intr (ndarray): The camera intrinsics matrix of shape (3, 3).
      cam_pose (ndarray): The camera pose (i.e. extrinsics) of shape (4, 4).
      obs_weight (float): The weight to assign for the current observation. A higher
        value
    """
        im_h, im_w = depth_im.shape
        if debug:
            new_classes = np.unique(class_im[..., 1])
            old_classes = new_classes

            if (debug_classes):
                print("Current classes in image to fuse: ",  [labels.get(str(int(n))) for n in new_classes], " - ", new_classes)
                _, _, class_vol = self.get_volume()
                scores = np.floor(class_vol / self._color_const)
                class_numbers = np.floor((class_vol - np.floor(class_vol / self._color_const) * self._color_const) / 256)
                numbers = np.unique(class_numbers)

                old_classes = np.hstack([old_classes, numbers])

                print("Current classes in voxel storage (before fusing): ", [labels.get(str(int(n))) for n in numbers], " - ", numbers)
                for numb in numbers:
                    if(numb == 0):
                        continue
                    label = labels.get(str(int(numb)), "unknown")

                    class_mask = class_numbers == numb
                    #score = scores[class_mask] Changing score
                    score = scores[class_mask]
                    print("Class  " + label +" (", numb, ") #voxel: ", np.sum(score > 0),  " scores: ", np.round(np.unique(score)/255, decimals=2))



        # Fold RGB color image into a single channel image
        color_im = color_im.astype(np.float32)
        color_im = np.floor(color_im[..., 2] * self._color_const + color_im[..., 1] * 256 + color_im[..., 0])

        # Fold class color image into a single channel image
        class_im = class_im.astype(np.float32)
        class_im = np.floor(class_im[..., 2] * self._color_const + class_im[..., 1] * 256 + class_im[..., 0])


        print("frame: ", self.frame)
        self.frame += 1

        if self.gpu_mode:  # GPU mode: integrate voxel volume (calls CUDA kernel)
            for gpu_loop_idx in range(self._n_gpu_loops):
                self._cuda_integrate(self._tsdf_vol_gpu,
                                     self._weight_vol_gpu,
                                     self._color_vol_gpu,
                                     self._class_vol_gpu,
                                     cuda.InOut(self._vol_dim.astype(np.float32)),
                                     cuda.InOut(self._vol_origin.astype(np.float32)),
                                     cuda.InOut(cam_intr.reshape(-1).astype(np.float32)),
                                     cuda.InOut(cam_pose.reshape(-1).astype(np.float32)),
                                     cuda.InOut(np.asarray([
                                         gpu_loop_idx,
                                         self._voxel_size,
                                         im_h,
                                         im_w,
                                         self._trunc_margin,
                                         obs_weight
                                     ], np.float32)),
                                     cuda.InOut(color_im.reshape(-1).astype(np.float32)),
                                     cuda.InOut(depth_im.reshape(-1).astype(np.float32)),
                                     cuda.InOut(class_im.reshape(-1).astype(np.float32)),
                                     block=(self._max_gpu_threads_per_block, 1, 1),
                                     grid=(
                                         int(self._max_gpu_grid_dim[0]),
                                         int(self._max_gpu_grid_dim[1]),
                                         int(self._max_gpu_grid_dim[2]),
                                     )
                                     )
                if self.filter_interval != -1 and (self.frame % self.filter_interval) == (self.filter_interval -1):
                    self.filterVolume()


        else:  # CPU mode: integrate voxel volume (vectorized implementation)
            # Convert voxel grid coordinates to pixel coordinates
            cam_pts = self.vox2world(self._vol_origin, self.vox_coords, self._voxel_size)
            cam_pts = rigid_transform(cam_pts, np.linalg.inv(cam_pose))
            pix_z = cam_pts[:, 2]
            pix = self.cam2pix(cam_pts, cam_intr)
            pix_x, pix_y = pix[:, 0], pix[:, 1]

            # Eliminate pixels outside view frustum
            valid_pix = np.logical_and(pix_x >= 0,
                                       np.logical_and(pix_x < im_w,
                                                      np.logical_and(pix_y >= 0,
                                                                     np.logical_and(pix_y < im_h,
                                                                                    pix_z > 0))))
            depth_val = np.zeros(pix_x.shape)
            depth_val[valid_pix] = depth_im[pix_y[valid_pix], pix_x[valid_pix]]

            # Integrate TSDF
            depth_diff = depth_val - pix_z
            valid_pts = np.logical_and(depth_val > 0, depth_diff >= -self._trunc_margin)
            dist = np.minimum(1, depth_diff / self._trunc_margin)
            valid_vox_x = self.vox_coords[valid_pts, 0]
            valid_vox_y = self.vox_coords[valid_pts, 1]
            valid_vox_z = self.vox_coords[valid_pts, 2]
            w_old = self._weight_vol_cpu[valid_vox_x, valid_vox_y, valid_vox_z]
            tsdf_vals = self._tsdf_vol_cpu[valid_vox_x, valid_vox_y, valid_vox_z]
            valid_dist = dist[valid_pts]
            tsdf_vol_new, w_new = self.integrate_tsdf(tsdf_vals, valid_dist, w_old, obs_weight)
            self._weight_vol_cpu[valid_vox_x, valid_vox_y, valid_vox_z] = w_new
            self._tsdf_vol_cpu[valid_vox_x, valid_vox_y, valid_vox_z] = tsdf_vol_new

            # Integrate color
            old_color = self._color_vol_cpu[valid_vox_x, valid_vox_y, valid_vox_z]
            old_b = np.floor(old_color / self._color_const)
            old_g = np.floor((old_color - old_b * self._color_const) / 256)
            old_r = old_color - old_b * self._color_const - old_g * 256
            new_color = color_im[pix_y[valid_pts], pix_x[valid_pts]]
            new_b = np.floor(new_color / self._color_const)
            new_g = np.floor((new_color - new_b * self._color_const) / 256)
            new_r = new_color - new_b * self._color_const - new_g * 256
            new_b = np.minimum(255., np.round((w_old * old_b + obs_weight * new_b) / w_new))
            new_g = np.minimum(255., np.round((w_old * old_g + obs_weight * new_g) / w_new))
            new_r = np.minimum(255., np.round((w_old * old_r + obs_weight * new_r) / w_new))
            self._color_vol_cpu[valid_vox_x, valid_vox_y, valid_vox_z] = new_b * self._color_const + new_g * 256 + new_r

            # Integrate class and scores
            class_numbers_score = self._class_vol_cpu[valid_vox_x, valid_vox_y, valid_vox_z]

            old_score = np.floor(class_numbers_score / self._color_const)
            old_label = np.floor((class_numbers_score - old_score * self._color_const) / 256)

            new_class_numbers_score = class_im[pix_y[valid_pts], pix_x[valid_pts]]
            new_score = np.floor(new_class_numbers_score / self._color_const)
            new_label = np.floor((new_class_numbers_score - new_score * self._color_const) / 256)

            """

            if (old_label == 0.0f | | old_score == 0.0f) {

            } else if (new_label =! 0.0f && old_label == new_label    AND not (old_label == 0.0f | | old_score == 0.0f)) {
            new_score = fmin((old_score + new_score), 255.0f);
            } else if(new_label =! 0.0f AND not  old_label == new_label  AND not (old_label == 0.0f | | old_score == 0.0f)) {
            new_score = fmax((old_score - new_score), 0.0f);
            }

            class_vol[voxel_idx] = new_score*256*256+new_label*256;
                """

            zero = np.logical_or(old_label == 0, old_score == 0)
            skip_these = np.logical_or(old_label == 0, old_score == 0)
            not_skip_these = np.logical_not(skip_these)

            same_label = np.logical_and(np.logical_and(new_label == old_label, np.logical_not(zero)), not_skip_these)

            diff_label = np.logical_and(np.logical_and(new_label != old_label, np.logical_not(zero)), not_skip_these)

            new_score[same_label] = np.minimum(old_score[same_label] + new_score[same_label], 255)
            new_score[diff_label] = np.maximum(old_score[diff_label] - new_score[diff_label], 0)

            # new_label[skip_these] = old_label[skip_these]
            # new_score[skip_these] = old_score[skip_these]

            self._class_vol_cpu[valid_vox_x, valid_vox_y, valid_vox_z] = new_score * self._color_const + new_label * 256

        if debug:

            _, _, class_vol = self.get_volume()
            class_numbers = np.floor((class_vol - np.floor(class_vol / self._color_const) * self._color_const) / 256)
            new_classes_voxel = np.unique(class_numbers)
            if debug_classes:
                print("Current classes in voxel storage (after fusing): ", [labels.get(str(int(n))) for n in new_classes_voxel], " - ", new_classes_voxel)

            if checkDuplicate:
                for c in new_classes_voxel:
                    if c not in old_classes:
                        print("-------------------------------------")
                        print("----!!! found newly generated class !!!! -----")
                        print("----      "+ str(c) +  "  -----")
                        print("")
                        print("-------------------------------------")

        if usePC:
            t1 = time.time()
            pc = self.get_point_cloud_color()
            print("getPC took", time.time() - t1)
            ptcloud_centred = pcl.PointCloud_PointXYZRGB()
            ptcloud_centred.from_array(pc)
            self.visual.ShowColorCloud(ptcloud_centred, b'cloud')

    def get_volume(self):
        if self.gpu_mode:
            cuda.memcpy_dtoh(self._tsdf_vol_cpu, self._tsdf_vol_gpu)
            cuda.memcpy_dtoh(self._color_vol_cpu, self._color_vol_gpu)
            cuda.memcpy_dtoh(self._class_vol_cpu, self._class_vol_gpu)
        else:
            # Fix for cpu
            return self._tsdf_vol_cpu.copy(), self._color_vol_cpu.copy(), self._class_vol_cpu.copy()

        return self._tsdf_vol_cpu, self._color_vol_cpu, self._class_vol_cpu

    def get_volume_2(self):
        if self.gpu_mode:
            cuda.memcpy_dtoh(self._tsdf_vol_cpu, self._tsdf_vol_gpu)
            cuda.memcpy_dtoh(self._color_vol_cpu, self._color_vol_gpu)
            cuda.memcpy_dtoh(self._class_vol_cpu, self._class_vol_gpu)
            cuda.memcpy_dtoh(self._weight_vol_cpu, self._weight_vol_gpu)
        else:
            # Fix for cpu
            return self._tsdf_vol_cpu.copy(), self._color_vol_cpu.copy(), self._class_vol_cpu.copy(), self._weight_vol_cpu.copy()

        return self._tsdf_vol_cpu, self._color_vol_cpu, self._class_vol_cpu, self._weight_vol_cpu

    def get_point_cloud(self):
        """Extract a point cloud from the voxel volume."""
        tsdf_vol, color_vol, _ = self.get_volume()

        # Marching cubes
        verts = measure.marching_cubes_lewiner(tsdf_vol, level=0)[0]
        verts_ind = np.round(verts).astype(int)
        verts = verts * self._voxel_size + self._vol_origin

        # Get vertex colors
        rgb_vals = color_vol[verts_ind[:, 0], verts_ind[:, 1], verts_ind[:, 2]]
        colors_b = np.floor(rgb_vals / self._color_const)
        colors_g = np.floor((rgb_vals - colors_b * self._color_const) / 256)
        colors_r = rgb_vals - colors_b * self._color_const - colors_g * 256
        colors = np.floor(np.asarray([colors_r, colors_g, colors_b])).T
        colors = colors.astype(np.uint8)

        pc = np.hstack([verts, colors])
        return pc

    def get_point_cloud_color(self):
        """Extract a point cloud from the voxel volume.
    """
        tsdf_vol, color_vol, class_vol = self.get_volume()

        # Marching cubes
        verts = measure.marching_cubes_lewiner(tsdf_vol, level=0)[0]
        verts_ind = np.round(verts).astype(int)
        verts = verts * self._voxel_size + self._vol_origin

        class_label_score = class_vol[verts_ind[:, 0], verts_ind[:, 1], verts_ind[:, 2]]

        scores = np.floor(class_label_score / self._color_const).astype(np.float32)
        class_numbers = np.floor((class_label_score - scores * self._color_const) / 256)

        colors = scores*255*255 + class_numbers

        mask = np.logical_and(class_numbers != 0, scores > 0.1)

        verts = verts[mask]
        h = class_numbers[mask]/255
        s = scores[mask]/255

        rgb = hsv_to_rgb(h,s,np.ones_like(h))

        rgb = rgb*0 + 255

        #rgb = (rgb[:,0]*256*256 + rgb[:,1]*256 + rgb[:,2]).astype(np.float32)
        rgb = (rgb[:, 0] * 256).astype(np.float32)

        pc = np.hstack([verts,  rgb.reshape(verts.shape[0], 1)])
        return pc

    def get_point_cloud_no_color(self):
        """Extract a point cloud from the voxel volume.
    """
        tsdf_vol, color_vol, _ = self.get_volume()

        # Marching cubes
        verts = measure.marching_cubes_lewiner(tsdf_vol, level=0)[0]
        verts_ind = np.round(verts).astype(int)
        verts = verts * self._voxel_size + self._vol_origin
        return verts

    def get_point_cloud_for_instance(self, class_number):
        """Extract a point cloud from the voxel volume.
        class_number: None -> all points that correspond to objects! will be extracted, otherwise only points matching the given class numbers
        """
        t1 = time.time()
        tsdf_vol, color_vol, class_vol = self.get_volume()
        print("getVolume took", time.time() - t1)
        # Marching cubes
        t1 = time.time()
        verts = measure.marching_cubes_lewiner(tsdf_vol, level=0)[0]
        verts_ind = np.round(verts).astype(int)
        verts = verts * self._voxel_size + self._vol_origin
        print("marching_cubes_lewiner took", time.time() - t1)

        t1 = time.time()
        # Get vertex colors
        rgb_vals = color_vol[verts_ind[:, 0], verts_ind[:, 1], verts_ind[:, 2]]
        colors_b = np.floor(rgb_vals / self._color_const)
        colors_g = np.floor((rgb_vals - colors_b * self._color_const) / 256)
        colors_r = rgb_vals - colors_b * self._color_const - colors_g * 256
        colors = np.floor(np.asarray([colors_r, colors_g, colors_b])).T
        colors = colors.astype(np.uint8)

        # Get class numbers and scores for each boxel
        class_label_score = class_vol[verts_ind[:, 0], verts_ind[:, 1], verts_ind[:, 2]]

        scores = np.floor(class_label_score / self._color_const)
        class_numbers = np.floor((class_label_score - scores * self._color_const) / 256)

        class_label_score = np.floor(np.asarray([class_numbers, scores])).T
        class_label_score = class_label_score.astype(np.uint8)

        if (class_number is not None):
            class_mask = class_numbers != class_number
            m = class_numbers == class_number
        else:
            class_mask = class_numbers == 0
            m = class_numbers != 0

        # Hide points that are not part of this object
        verts[class_mask] = 0
        colors[class_mask] = 0

        print("masking took", time.time() - t1)
        if np.max(colors) == 0:
            raise ValueError("class not found")

        pc =  np.hstack([verts[m], colors[m]])

        #pc = np.hstack([verts, colors])
        return pc
    
    def get_subinstance(self,verts,class_numb): 
        #clusters all vert-groups together(distance between two verts smaller than 0.05 = same Instance)
        instance = trimesh.grouping.clusters(verts, 0.05) 
        num_of_ins = len(instance)  
        print("For the class", class_numb," ",num_of_ins,"instances are found.")
        return instance

    def get_mesh(self, labels, _class=False, col_dict={}, score_threshold=0.1):
        tsdf, color, class_vol= self.get_volume()
        return self.get_mesh_for_volume(tsdf, color, class_vol, labels, _class=_class, col_dict=col_dict, score_threshold=score_threshold)

    def get_mesh_for_volume(self,  tsdf_vol, color_vol, class_vol , labels, _class=False, col_dict={}, score_threshold=0.1):
        """Compute a mesh from the voxel volume using marching cubes.
            _class: if set to true, points will be colored according to the class
            col_dict: dict containing mapping class_number -> color
            score_threshold: only points with score above this threshold will be interpreted as a given class
        """
        not_found = []

        def filter(array):
            return mode(array)[0][0]

        def getColor(_class, score):
            if score < score_t:
                return (0, 0, 0)

            col = col_dict.get(int(_class), None)
            if col == None:
                col = (255, 255, 255)
                if _class not in not_found:
                    not_found.append(_class)
                    print("Color for class not found: ", labels.get(str(_class)), "(", _class, ")")

            return col

        score_t = 256 * score_threshold

        # Marching cubes
        verts, faces, norms, vals = measure.marching_cubes_lewiner(tsdf_vol, level=0)
        verts_ind = np.round(verts).astype(int)

        # Get vertex colors
        rgb_vals = color_vol[verts_ind[:, 0], verts_ind[:, 1], verts_ind[:, 2]]
        colors_b = np.floor(rgb_vals / self._color_const)
        colors_g = np.floor((rgb_vals - colors_b * self._color_const) / 256)
        colors_r = rgb_vals - colors_b * self._color_const - colors_g * 256

        colors_orig = np.floor(np.asarray([colors_r, colors_g, colors_b])).T
        colors_orig = colors_orig.astype(np.uint8)

        if _class:
            # Get class numbers and scores for each boxel

            # filter class vol

            """print("filter class volume")

            scores = np.floor(class_vol / self._color_const)
            class_numbers = np.floor((class_vol - scores * self._color_const) / 256)

            #class_numbers = ndimage.filters.generic_filter(class_numbers, filter, footprint=morphology.ball(4))
            class_numbers = ndimage.median_filter(class_numbers, footprint = morphology.ball(2))

            print("numbers", np.unique(class_numbers))"""
            #class_numbers = ndimage.median_filter(class_numbers, size = 4)

            scores = np.floor(class_vol / self._color_const)
            class_numbers = np.floor((class_vol - scores * self._color_const) / 256)
            #class_label_score = class_vol[verts_ind[:, 0], verts_ind[:, 1], verts_ind[:, 2]]
            scores = scores[verts_ind[:, 0], verts_ind[:, 1], verts_ind[:, 2]]
            class_numbers = class_numbers[verts_ind[:, 0], verts_ind[:, 1], verts_ind[:, 2]]

            print("zeros", (class_numbers == 0).shape)
            #scores = np.floor(class_label_score / self._color_const)
            #class_numbers = np.floor((class_label_score - scores * self._color_const) / 256)

            class_label_score = np.floor(np.asarray([class_numbers, scores])).T
            class_label_score = class_label_score.astype(np.uint8)

            print("found the following classes:")
            numbs = np.unique(class_label_score[:, 0])
            print([labels.get(str(c)) for c in numbs], " - ", numbs)

            # map each voxel to a color from the color dict
            colors = np.array([getColor(r[0], r[1]) for r in class_label_score])
            colors = colors.astype(np.uint8)
        else:
            colors = colors_orig

        verts = verts * self._voxel_size + self._vol_origin  # voxel grid coordinates to world coordinates
        return verts, faces, norms, colors, colors_orig, class_numbers

    def get_mesh_for_instance(self,labels, class_number=None, col_dict={}, score_threshold=0.1):
        """Compute a mesh from the voxel volume using marching cubes.
            _class: if set to true, points will be colored according to the class
            cold_cit: dict containing mapping class_number -> color
            score_threshold: only points with score above this threshold will be interpreted as a given class
        """
        not_found = []
        def getColor(_class, score):
            if score < score_t:
                return (0, 0, 0)

            col = col_dict.get(int(_class), None)
            if col == None:
                col = (255, 255, 255)
                if _class not in not_found:
                    not_found.append(_class)
                    print("Color for class not found: ", labels.get(str(_class)), "(", _class, ")")

            return col


        tsdf_vol, color_vol, class_vol = self.get_volume()
        score_t = 256 * score_threshold

        scores = np.floor(class_vol / self._color_const)
        class_numbers = np.floor((class_vol - scores * self._color_const) / 256)
        print("numbers, ",np.unique(class_numbers))
        if(class_number is None):
            tsdf_vol[class_numbers == 0] = 1
        else:
            tsdf_vol[class_numbers != class_number] = 1
            tsdf_vol[scores < score_t] = 1

        if(np.min(tsdf_vol) == 1):
            raise ValueError("class not found")

        scores = np.floor(class_vol / self._color_const)
        class_numbers = np.floor((class_vol - scores * self._color_const) / 256)
        tsdf_vol[class_numbers == 0] = 1
        # Marching cubes
        verts, faces, norms, vals = measure.marching_cubes_lewiner(tsdf_vol, level=0)
        verts_ind = np.round(verts).astype(int)


        # Get class numbers and scores for each boxel
        class_label_score = class_vol[verts_ind[:, 0], verts_ind[:, 1], verts_ind[:, 2]]

        scores = np.floor(class_label_score / self._color_const)
        class_numbers = np.floor((class_label_score - scores * self._color_const) / 256)

        class_label_score = np.floor(np.asarray([class_numbers, scores])).T
        class_label_score = class_label_score.astype(np.uint8)

        print("found the following classes:")
        numbs = np.unique(class_label_score[:, 0])
        print([labels.get(str(c)) for c in numbs], " - ", numbs)

        # map each voxel to a color from the color dict
        colors = np.array([getColor(r[0], r[1]) for r in class_label_score])
        colors = colors.astype(np.uint8)

        # Get vertex colors
        rgb_vals = color_vol[verts_ind[:, 0], verts_ind[:, 1], verts_ind[:, 2]]
        colors_b = np.floor(rgb_vals / self._color_const)
        colors_g = np.floor((rgb_vals - colors_b * self._color_const) / 256)
        colors_r = rgb_vals - colors_b * self._color_const - colors_g * 256

        colors_orig = np.floor(np.asarray([colors_r, colors_g, colors_b])).T
        colors_orig = colors_orig.astype(np.uint8)

        verts = verts * self._voxel_size + self._vol_origin  # voxel grid coordinates to world coordinates

        return verts, faces, norms, colors, colors_orig, class_numbers

def get_mesh_for_subinstance(verts,faces,norms,colors,label,distance, min_size):
    clust = trimesh.grouping.clusters(verts, distance)
    nofcl = len(clust)
    counter = 0
    for i in range(nofcl):
        actins = clust[i]
        if len(actins)>=min_size:
            counter +=1
            face = []
            for f in range(len(faces)):
                if faces[f,0] in actins:
                    if faces[f,1] in actins:
                        if faces[f,2] in actins:
                            face.append(faces[f])
            face = np.array(face)
            print("Class",label,"-->create mesh",str(counter))
            meshwrite("output/mesh_" + str(label) + str(counter) +"_pc.ply", verts, face, norms, colors)
            
    print("Class",label,"has",str(counter),"instances.")

def rigid_transform(xyz, transform):
    """Applies a rigid transform to an (N, 3) pointcloud.
  """
    xyz_h = np.hstack([xyz, np.ones((len(xyz), 1), dtype=np.float32)])
    xyz_t_h = np.dot(transform, xyz_h.T).T
    return xyz_t_h[:, :3]


def get_view_frustum(depth_im, cam_intr, cam_pose):
    """Get corners of 3D camera view frustum of depth image
  """
    im_h = depth_im.shape[0]
    im_w = depth_im.shape[1]
    max_depth = np.max(depth_im)
    view_frust_pts = np.array([
        (np.array([0, 0, 0, im_w, im_w]) - cam_intr[0, 2]) * np.array([0, max_depth, max_depth, max_depth, max_depth]) /
        cam_intr[0, 0],
        (np.array([0, 0, im_h, 0, im_h]) - cam_intr[1, 2]) * np.array([0, max_depth, max_depth, max_depth, max_depth]) /
        cam_intr[1, 1],
        np.array([0, max_depth, max_depth, max_depth, max_depth])
    ])
    view_frust_pts = rigid_transform(view_frust_pts.T, cam_pose).T
    return view_frust_pts


def meshwrite(filename, verts, faces, norms, colors):
    """Save a 3D mesh to a polygon .ply file.
  """
    # Write header
    ply_file = open(filename, 'w')
    ply_file.write("ply\n")
    ply_file.write("format ascii 1.0\n")
    ply_file.write("element vertex %d\n" % (verts.shape[0]))
    ply_file.write("property float x\n")
    ply_file.write("property float y\n")
    ply_file.write("property float z\n")
    ply_file.write("property float nx\n")
    ply_file.write("property float ny\n")
    ply_file.write("property float nz\n")
    ply_file.write("property uchar red\n")
    ply_file.write("property uchar green\n")
    ply_file.write("property uchar blue\n")
    ply_file.write("element face %d\n" % (faces.shape[0]))
    ply_file.write("property list uchar int vertex_index\n")
    ply_file.write("end_header\n")

    # Write vertex list
    for i in range(verts.shape[0]):
        ply_file.write("%f %f %f %f %f %f %d %d %d\n" % (
            verts[i, 0], verts[i, 1], verts[i, 2],
            norms[i, 0], norms[i, 1], norms[i, 2],
            colors[i, 0], colors[i, 1], colors[i, 2],
        ))

    # Write face list
    for i in range(faces.shape[0]):
        ply_file.write("3 %d %d %d\n" % (faces[i, 0], faces[i, 1], faces[i, 2]))

    ply_file.close()


def pcwrite(filename, xyzrgb):
    """Save a point cloud to a polygon .ply file.
  """
    xyz = xyzrgb[:, :3]
    rgb = xyzrgb[:, 3:].astype(np.uint8)

    # Write header
    ply_file = open(filename, 'w')
    ply_file.write("ply\n")
    ply_file.write("format ascii 1.0\n")
    ply_file.write("element vertex %d\n" % (xyz.shape[0]))
    ply_file.write("property float x\n")
    ply_file.write("property float y\n")
    ply_file.write("property float z\n")
    ply_file.write("property uchar red\n")
    ply_file.write("property uchar green\n")
    ply_file.write("property uchar blue\n")
    ply_file.write("end_header\n")

    # Write vertex list
    for i in range(xyz.shape[0]):

        ply_file.write("%f %f %f %d %d %d\n" % (
            xyz[i, 0], xyz[i, 1], xyz[i, 2],
            rgb[i, 0], rgb[i, 1], rgb[i, 2],
        ))
