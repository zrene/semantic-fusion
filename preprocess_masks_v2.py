from PIL import Image, ImageDraw

import os
import cv2
import csv
import numpy as np

import re
# color_dict = {
#         0: (255, 0, 0),
#         1: (0, 255, 0),
#         2: (0, 0, 255),
#         3: (255, 0, 255),
#         4: (0, 255, 255),
#         5: (139,69,19),
#         6: (139,69,19),
#         7: (255, 255, 0),
#         9: (255, 20,147),
#         10: (147, 0,147),
#         11: (147, 147,0),
#         12: (147, 147,147),
#         13: (255, 255,255)
#     }
#
# map = {
#         'Unknown': 0,
#         'Bed': 1,
#         'Books': 2,
#         'Ceiling': 3,
#         "Chair": 4,
#         "Floor": 5,
#         "Furniture": 6,
#         "Object": 7,
#         "Picture": 8,
#         "Sofa": 9,
#         "Table": 10,
#         "TV": 11,
#         "Wall": 12,
#         "Window": 13,
#     }

def getColorForLabel(label, color_dict, map):
    """ Returns a color for a class label """
    return color_dict.get(map.get(label, 0))


# Restrict ourself to this classes

allowed_classes = [-1] #[0,1,2,3,4,5,6,7,8,9,10,11,12,13,40]

pattern = re.compile("\d+_\d+_.+")

def load_result(path):
    """ Loads masks, labels and scores from the output folder"""
    masks = []
    labels = []
    scores = []

    for name in os.listdir(path):
        if pattern.match(name):
            mask = Image.open(path + "/" + name)
            n = name.split("_")

            if len(n) < 3:
                continue

            label = int(n[1])
            score = float(n[2][:-4])

            masks.append(mask)
            labels.append(label)
            scores.append(score)

    # order everything be score. lowest score first
    index_scores = np.array([np.arange(len(masks)), scores]).T
    index_scores = index_scores[index_scores[:, 1].argsort()]

    masks_sorted = []
    labels_sorted = []
    scores_sorted = []

    for ind in index_scores[:,0]:
        ind = int(ind)

        masks_sorted.append(masks[ind])
        labels_sorted.append(labels[ind])
        scores_sorted.append(scores[ind])


    return masks_sorted, labels_sorted, scores_sorted


def get_classes(path):
    """ Gets mapping class number <-> class label"""

    with open(path, newline='') as csvfile:
        labelreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        labels = {}
        mapping = {}
        color_dict = {}
        for row in labelreader:
            labels[row[0]] = row[1]
            mapping[row[1]] = int(row[0])
            color_dict[int(row[0])] = (int(row[2]), int(row[3]), int(row[4]))
    return labels, mapping, color_dict



def preprocess_all(mask_path, data_path, classes, mapping, color_dict, score_threshold = 0.5, morph_preprocessing = False):
    """ Converts mask images into class_score image and creates a color image using the masks.
        Class_score image: Just a regular RGB image, containing the class number as G, and the Score as B value
        Color image: RGB image, input image with masks as overlay. Masks are colored according to their class """

    print("starting preprocessing...")
    for file in os.listdir(mask_path):
        # print("Going to convert masks from file: " + file)
        #
        # print("Creating image containing class and score information")
        create_class_image(mask_path + "/" + file, mask_path + "/" + file + "/class_image.png",
                           Image.open(data_path + "/" + file + ".jpg").convert("RGBA"),
                           classes = classes, allowed_classes = allowed_classes,
                           score_threshold = score_threshold,
                           morph_preprocessing = morph_preprocessing )

        # print("Creating color model")
        create_image_with_class_color(mask_path + "/" + file, mask_path + "/" + file + "/color_image.jpg",
                                      Image.open(data_path + "/" + file + ".jpg").convert("RGBA"),
                                      classes= classes,
                                      color_dict = color_dict,
                                      map = mapping,
                                      allowed_classes = allowed_classes,
                                      score_threshold = score_threshold,
                                      morph_preprocessing = morph_preprocessing )
    print("done preprocessing")

def create_class_image(path, output_path, input, classes, allowed_classes, mask_threshold = 0.5, score_threshold = 0.8, morph_preprocessing  = True):
    """ Creates a class image: Just a regular RGB image, containing the class number as G, and the Score as B value"""
    masks, labels, scores = load_result(path)

    # Create a new image to draw on
    box_image = Image.new('RGB', input.size, (0, 0, 0))

    # Convert to numpy array, since using Pillow created artifacts while filling bitmap
    img = np.asarray(box_image).copy().astype(int)

    # All pixels in the mask with a value bigger than this threshold are
    # assigned to the binary mask.

    # All detections with a score bigger than this threshold are used as actual
    # detections.

    # Extract predicted boxes and masks.
    for i in range(len(masks)):

        mask = masks[i]
        score = scores[i]
        label = labels[i]
        score = scores[i]

        # Check if we want to create an instance from this class
        class_name = classes[str(label)]
        if (score < score_threshold) or ( (int(label) not in allowed_classes) and -1 not in allowed_classes):
            # print(class_name, "(blocked) number: ", label)
            continue

        # print(class_name)
        # print("got: ", label)
        # print("score ", score)
        # #
        # # Convert score to a value [0,256]
        # value = int(score * 256)
        # print("value: ", value)

        # This does not work, since pillow somehow interpolates or sth and values will get smeared out
        # box_image_draw.bitmap((0, 0), mask, fill=fill)

        # Only keep values of masks that are higher than this threshold
        # (problem with jpg. compression creates artifacts)
        # which lead to the image not being pure binary
        threshold = 200
        mask_arr = np.asarray(mask)
        if morph_preprocessing:
            mask_arr = morphological_processing(mask_arr)





        # Set G channel of this mask to the value of the class

        img[(mask_arr > threshold), 1] = label
        # Set B channel of this mask to the score of the prediction
        img[(mask_arr > threshold) , 0] = int(score * 255)

    cv2.imwrite(output_path, img)
#erodes and dilates the masks to make the edges smoother and reduce bleeding
def morphological_processing(mask_arr, kernel_size = (10,10)):
    kernel = np.ones(kernel_size, np.uint8)
    for i in range(5):
        mask_arr = cv2.erode(mask_arr, kernel)
        mask_arr = cv2.dilate(mask_arr, kernel)

    mask_arr = cv2.erode(mask_arr, kernel)
    return mask_arr

def create_image_with_class_color(path, output_path, input, classes, allowed_classes, color_dict, map, score_threshold = 0.5, morph_preprocessing = False):
    """Color image: RGB image, input image with masks as overlay. Masks are colored according to their class """
    masks, labels, scores = load_result(path)

    box_image = Image.new('RGBA', input.size, (255, 255, 255, 0))
    box_image_draw = ImageDraw.Draw(box_image)

    # Extract predicted boxes and masks.
    for i in range(len(masks)):
        mask = masks[i]
        label = labels[i]
        score = scores[i]
        mask_arr = np.asarray(mask)
        if morph_preprocessing:
            mask_arr = morphological_processing(mask_arr)

        if (score < score_threshold) or ( (int(label) not in allowed_classes) and -1 not in allowed_classes):
            continue
        mask = Image.fromarray(mask_arr)
        label = classes[str(label)]
        box_image_draw.bitmap((0, 0), mask, fill=getColorForLabel(label, color_dict, map))

    # Save the predicted test image.
    out = Image.alpha_composite(input, box_image).convert("RGB")
    out.save(output_path, "JPEG")

