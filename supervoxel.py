import pclpy
from pclpy import pcl
import pclpy.pcl.segmentation
from pclpy import utils
import sys, inspect
import time
import random
import math
import time
import pickle
import numpy as np
import fusion
import open3d as o3d
from scipy.spatial import distance
import threading



# Which class should get assigned which color?
# color_dict = {
#     66: (255, 0, 0),
#     15: (0, 255, 0),
#     135: (0, 0, 255),
#     2: (255, 0, 255),
#     21: (0, 255, 255),
#     36: (139, 69, 19),  # desk,,
#     19: (139, 69, 19),  # desk
#     119: (255, 255, 0),
#     1: (255, 20, 147)  # book
# }

color_dict = {
        0: (255, 0, 0),
        1: (0, 255, 0),
        2: (0, 0, 255),
        3: (255, 0, 255),
        4: (0, 255, 255),
        5: (139,69,19),
        6: (139,69,19),
        7: (255, 255, 0),
        9: (255, 20,147),
        10: (147, 0,147),
        11: (147, 147,0),
        12: (147, 147,147),
        13: (255, 255,255)
    }


def closest_point(lookup_point, all_points):
    """ lookup_point: a n-dimensional point
        all_points: an array with n-dimensional points
        :return the index of the node in the nodes array that is closes to the given point"""

    closest_index = distance.cdist([lookup_point], all_points).argmin()
    return closest_index


class Supervoxel:
    """ Class representing a supervoxel segemntation for the tsdf volume """
    def __init__(self, volume, color_dict, classes, seed_resolution=0.2, spatial_importance=1.0, normal_importance=1.0, color_importance=0.7):
        self.volume = volume
        self.seed_resolution = seed_resolution

        self.spatial_importance = spatial_importance
        self.normal_importance = normal_importance
        self.color_importance = color_importance

        # Segmented point cloud (-> labels as colors after supervoxel creation)
        self.pc_segmented = pcl.PointCloud.PointXYZRGBL()

        # Supervoxel point cloud (-> Shows different supervoxel in diffrent colors)
        self.pc_supervoxel = pcl.PointCloud.PointXYZRGBL()

        # Points of the point cloud from the tsdf volume
        self.verts = None
        # Colors obtained from TSDF Volume (Class dependant)
        self.colors = None

        # Colors as they originally were in the images
        self.colors_orig= None

        # tsdf volume information. cached here, because they can only be obtained in a main thread
        self.tsdf_vol = None
        self.color_vol = None
        self.class_vol = None
        self.color_dict = color_dict
        self.classes = classes

    def extract_volume(self):
        """ Extracts tsdf volume from GPU.
            This function must be run in the main thread, otherwise CUDA fails
        """
        self.tsdf_vol,  self.color_vol,  self.class_vol = self.volume.get_volume()

    def extract_point_cloud(self):
        """
        Extracts a point cloud from the tsdf volume.
        This function does not use cuda (sadly :( ) and can thus be used if a thread that is not the main thread

        The extracted clouds are stored in the self.pc_segmented and self.pc_supervoxel
        """

        print("extracting point cloud from Voxels")
        # TODO optimize using cuda
        t = time.time()
        self.verts, self.faces, self.norms, self.colors, self.colors_orig, self.class_numbers = self.volume.get_mesh_for_volume(self.tsdf_vol, self.color_vol, self.class_vol, _class=True, col_dict=self.color_dict, labels = self.classes)
        diff = time.time() - t
        print("took", diff, "s")

        print("Converting to pcl point cloud")
        t = time.time()

        self.pc_segmented_not_seg = pclpy.pcl.PointCloud.PointXYZRGBA.from_array(self.verts, self.colors)
        self.pc_segmented_not_seg_orig_color = pclpy.pcl.PointCloud.PointXYZRGBA.from_array(self.verts, self.colors_orig)
        self.pc_normals = pcl.PointCloud.Normal.from_array(self.norms)

        diff = time.time() - t
        print("took", diff, "s")

        return self.pc_segmented_not_seg, self.pc_segmented_not_seg_orig_color

    def extract_supervoxels(self):

        pc = self.pc_segmented_not_seg_orig_color
        verts = self.verts
        colors = self.colors

        print("Extracting supervoxel")
        t = time.time()
        vox = pcl.segmentation.SupervoxelClustering.PointXYZRGBA(voxel_resolution=0.02, seed_resolution=self.seed_resolution)

        vox.setInputCloud(pc)
        vox.setNormalCloud(self.pc_normals)

        vox.setUseSingleCameraTransform(False)

        vox.setSpatialImportance(self.spatial_importance)
        vox.setNormalImportance(self.normal_importance)
        vox.setColorImportance(self.color_importance)

        # Contains a mapping (supervoxel ID) <-> (Point Cloud RGBA)
        output = pcl.vectors.map_uint32t_PointXYZRGBA()
        vox.extract(output)
        diff = time.time() - t
        print("Extracting took", diff, "s")

        print("found: ", len(output), " Supervoxels")

        # Colors used to color supervoxel (Show voxel segmentation)
        color_list = [
            np.array([255,0,0]),
            np.array([0,255,0]),
            np.array([0,0,255]),
            np.array([255,0,255]),
            np.array([255,255,0])
        ]

        pc_n = pcl.PointCloud.PointXYZRGBL()
        pc_sv = pcl.PointCloud.PointXYZRGBL()

        t = time.time()

        points_sem_color = None
        points_real_color = None
        points = None


        id_to_pc_mapping = {}
        id_to_norms_mapping = {}
        id_to_seg_color_mapping = {}


        # For each center point of a supervoxel, obtain the neares label in the tsdf volume
        # Assign every point of the supervoxel the same label as the label of the center point
        for i in output:
            # Center point of this supervoxel
            centerPoint = pclpy.pcl.point_types.PointXYZRGBA()
            output[i].getCentroidPoint(centerPoint)

            # Find the nearest point in the point cloud from the tsdf volume (verts)
            ind = closest_point(np.array([centerPoint.x, centerPoint.y, centerPoint.z]), verts)
            # Color all points of this supervoxel with the color of this point (-> assign label)
            col = colors[ind]
            _class = int(self.class_numbers[ind])

            # Stack all points as big numpy array and convert them later into a point cloud (speed improvement)
            if points_real_color is None:
                points_real_color = np.full((output[i].voxels_.size(), 3), color_list[i%5])
                points_sem_color = np.full((output[i].voxels_.size(), 3), col)
                points = output[i].voxels_.xyz

                if _class not in id_to_pc_mapping:
                    id_to_pc_mapping[_class] = np.hstack([points, output[i].voxels_.rgb])
                    id_to_norms_mapping[_class] = output[i].normals_.normals
                    id_to_seg_color_mapping[_class] = np.full((output[i].voxels_.size(), 3), col)
                else:
                    id_to_pc_mapping[_class] = np.vstack(id_to_pc_mapping[_class], np.hstack(points,  output[i].voxels_.rgb))
                    id_to_norms_mapping[_class] = np.vstack([id_to_norms_mapping[_class], output[i].normals_.normals])
                    id_to_seg_color_mapping[_class] = np.vstack([id_to_seg_color_mapping[_class], np.full((output[i].voxels_.size(), 3), col)])
            else:
                points = np.vstack([points, output[i].voxels_.xyz])
                points_real_color = np.vstack([points_real_color, np.full((output[i].voxels_.size(), 3), color_list[i % 5])])
                points_sem_color = np.vstack([points_sem_color,   np.full((output[i].voxels_.size(), 3), col)])

                if _class not in id_to_pc_mapping:
                    id_to_pc_mapping[_class] = np.hstack([output[i].voxels_.xyz,output[i].voxels_.rgb])
                    id_to_norms_mapping[_class] = output[i].normals_.normals
                    id_to_seg_color_mapping[_class] = np.full((output[i].voxels_.size(), 3), col)
                else:
                    id_to_pc_mapping[_class] = np.vstack([id_to_pc_mapping[_class], np.hstack([output[i].voxels_.xyz, output[i].voxels_.rgb])])
                    id_to_norms_mapping[_class] = np.vstack([id_to_norms_mapping[_class], output[i].normals_.normals])
                    id_to_seg_color_mapping[_class] = np.vstack([id_to_seg_color_mapping[_class], np.full((output[i].voxels_.size(), 3), col)])

        pc_sv = pc_sv.from_array(points, points_real_color)
        pc_n = pc_n.from_array(points, points_sem_color)

        print("keys", id_to_pc_mapping.keys())

        diff = time.time() - t
        print("Coloring took", diff, "s")

        self.pc_segmented = pc_n
        self.pc_supervoxel = pc_sv

        self.id_to_pc_mapping = id_to_pc_mapping
        self.id_to_norms_mapping = id_to_norms_mapping
        self.id_to_seg_color_mapping = id_to_seg_color_mapping


    def get_pc_for_id(self, id):
        if id is None:
            j = 0
            for i in self.id_to_pc_mapping:
                if j == 0:
                    pc = self.id_to_pc_mapping[i]
                    normals = self.id_to_norms_mapping[i]
                    cols = self.id_to_seg_color_mapping[i]
                else:
                    pc = np.vstack([pc, self.id_to_pc_mapping[i]])
                    normals = np.vstack([normals, self.id_to_norms_mapping[i]])
                    cols = np.vstack([cols, self.id_to_seg_color_mapping[i]])
                j += 1

            return np.hstack([pc[:, 0:3], cols]), -normals

        if id not in self.id_to_pc_mapping:
            raise ValueError("id not stored in mapping")

        pts_rgb = self.id_to_pc_mapping[id]

        return self.id_to_pc_mapping[id], -self.id_to_norms_mapping[id]