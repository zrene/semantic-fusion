import time
import cv2
import numpy as np

import preprocess_masks_v2 as pre
import os

import fusion
import supervoxel
import pickle
import numpy as np
import threading
import open3d as o3d


class SemanticFuser():
    # Which class should get assigned which color?
    # color_dict = {
    #     66: (255, 0, 0),
    #     15: (0, 255, 0),
    #     135: (0, 0, 255),
    #     2: (255, 0, 255),
    #
    #     21: (0, 255, 255),
    #     36: (139,69,19),#desk,,
    #     19: (139,69,19),#desk
    #     119: (255, 255, 0),
    #     1: (255, 20,147) # book
    # }
    #
    # # Which class should be extracted to point clouds
    # classes_to_extract = {
    #     'Computer': 66,
    #     'Paper': 15,
    #     'Towel': 135,
    #     'Bottle': 2,
    #     "Wall" : 21,
    #     "Pillow": 119,
    #     "Desk" :  36,
    # }

    #new model
    # color_dict = {
    #     0: (255, 0, 0),
    #     1: (0, 255, 0),
    #     2: (0, 0, 255),
    #     3: (255, 0, 255),
    #     4: (0, 255, 255),
    #     5: (139, 69, 19),
    #     6: (139, 69, 19),
    #     7: (255, 255, 0),
    #     9: (255, 20, 147),
    #     10: (147, 0, 147),
    #     11: (147, 147, 0),
    #     12: (147, 147, 147),
    #     13: (255, 255, 255)
    # }
    # classes_to_extract = {
    #     'Unknown': 0,
    #     'Bed': 1,
    #     'Books': 2,
    #     'Ceiling': 3,
    #     "Chair": 4,
    #     "Floor": 5,
    #     "Furniture": 6,
    #     "Object": 7,
    #     "Picture": 8,
    #     "Sofa": 9,
    #     "Table": 10,
    #     "TV": 11,
    #     "Wall": 12,
    #     "Window": 13,
    # }
    #
    # classes = {
    #     "0": 'Unknown',
    #     "1": 'Bed',
    #     "2": 'Books',
    #     "3": 'Ceiling',
    #     "4": 'Chair',
    #     "5": 'Floor',
    #     "6": 'Furniture',
    #     "7": 'Object',
    #     "8": 'Picture',
    #     "9": 'Sofa',
    #     "10": 'Table',
    #     "11": 'TV',
    #     "12": 'Wall',
    #     "13": 'Window'
    # }


    def __init__(self, classes, mapping, color_dict, n_imgs = 10, start = 1651,
                 path_to_mask_images = "mask_data",
                 path_to_rgbd = "data",
                 reproccess_images = False, score_threshold = 0.5,
                 supervoxel_params = [0.2,1.0,1.0,0.7],
                 morph_preprocessing = False,
                 voxel_size = 0.03,
                 filter_interval = -1):

        self.n_imgs = n_imgs
        self.start = start
        self.path_to_mask_images = path_to_mask_images
        self.path_to_rgbd = path_to_rgbd
        self.color_dict = color_dict
        self.classes = classes

        self.supervoxel_params = supervoxel_params
        self.morph_preprocessing  = morph_preprocessing

        print("Estimating voxel volume bounds...")
        print(self.path_to_rgbd + "/camera-intrinsics.txt")
        self.cam_intr = np.loadtxt("camera-intrinsics.txt", delimiter=' ')
        vol_bnds = np.zeros((3, 2))

        for i in range(n_imgs):
            i = i + start
            # Read depth image and camera pose
            depth_im = cv2.imread(path_to_rgbd + "/%05d.png" % (i), -1).astype(float)
            depth_im /= 1000.  # depth is saved in 16-bit PNG in millimeters

            print(   np.max(depth_im))
            depth_im[depth_im == 65.535] = 0  # set invalid depth to 0 (specific to 7-scenes dataset)
            cam_pose = np.loadtxt(path_to_rgbd + "/%05d.txt" % (i))  # 4x4 rigid transformation matrix

            # Compute camera view frustum and extend convex hull
            view_frust_pts = fusion.get_view_frustum(depth_im, self.cam_intr, cam_pose)
            vol_bnds[:, 0] = np.minimum(vol_bnds[:, 0], np.amin(view_frust_pts, axis=1))
            vol_bnds[:, 1] = np.maximum(vol_bnds[:, 1], np.amax(view_frust_pts, axis=1))

            # Check if needed images are present. If not generate them
            if (reproccess_images or not (os.path.exists(path_to_mask_images + "/%05d" % (i) + "/color_image.jpg") and os.path.exists(path_to_mask_images + "/%05d" % (i) + "/class_image.png"))):
                print("color images or class image not found. Going to run preprocessing again")
                pre.preprocess_all(path_to_mask_images, path_to_rgbd, classes, mapping, color_dict, score_threshold = score_threshold, morph_preprocessing = self.morph_preprocessing )
                reproccess_images = False

        # ======================================================================================================== #



        # ======================================================================================================== #
        # Integrate
        # ======================================================================================================== #
        # Initialize voxel volume
        print("Initializing voxel volume...")
        self.tsdf_vol = fusion.TSDFVolume(vol_bnds, voxel_size=voxel_size, filter_interval = filter_interval, use_gpu=True)
        self.svox = supervoxel.Supervoxel(self.tsdf_vol,
                                          self.color_dict,
                                          self.classes,
                                          seed_resolution=self.supervoxel_params[0],
                                          spatial_importance = self.supervoxel_params[1],
                                          normal_importance= self.supervoxel_params[2],
                                          color_importance=self.supervoxel_params[3])


    def filter_volume(self):
        self.tsdf_vol.filterVolume()

    def fuse_images(self, imageNumber , debug = False):
        # Loop through RGB-D images and fuse them together
        t0_elapse = time.time()
        i = imageNumber

        i = i + self.start
        print("Fusing frame (%d) %d/%d" % (i, i-self.start, self.n_imgs))

        # Read RGB-D image and camera pose
        # color_image = cv2.cvtColor(cv2.imread("data/%05d.jpg"%(i)), cv2.COLOR_BGR2RGB)

        try: color_image = cv2.cvtColor(cv2.imread(self.path_to_mask_images + "/%05d/input.jpg" % (i)), cv2.COLOR_BGR2RGB)
        except: color_image = cv2.cvtColor(cv2.imread(self.path_to_mask_images + "/%05d/input.png" % (i)), cv2.COLOR_BGR2RGB)

        # Image containing class and score information RGB -> [0, class_number, score]
        class_img = cv2.cvtColor(cv2.imread(self.path_to_mask_images + "/%05d/class_image.png" % (i)), cv2.COLOR_BGR2RGB)


        depth_im = cv2.imread(self.path_to_rgbd +"/%05d.png" % (i), -1).astype(float)
        depth_im /= 1000.
        depth_im[depth_im == 65.535] = 0
        cam_pose = np.loadtxt(self.path_to_rgbd + "/%05d.txt" % (i))

        if(depth_im.shape[1] != class_img.shape[1] or depth_im.shape[0] != class_img.shape[0]):
            print("resizing images")
            class_img = cv2.resize(class_img, (depth_im.shape[1] , depth_im.shape[0]), interpolation=cv2.INTER_AREA)
            color_image = cv2.resize(color_image, (depth_im.shape[1], depth_im.shape[0]), interpolation=cv2.INTER_AREA)

        if(debug):
           cv2.imshow('image', cv2.resize(np.vstack([color_image, class_img]),(640,960)))
           # cv2.imshow('image2', color_image)
           # Get a numpy array to display from the simulation
           cv2.waitKey(20)
           input("Press Enter to continue...")


        # Integrate observation into voxel volume (assume color aligned with depth)
        self.tsdf_vol.integrate(color_image, depth_im, class_img, self.cam_intr, cam_pose, self.classes, obs_weight=1.)


    def createSuperVoxels(self):
        self.svox.extract_supervoxels()
        return self.svox.pc_supervoxel, self.svox.pc_segmented

    def prepareVoxelExtraction(self):
        self.svox.extract_volume()

    def extractPc(self):
        self.tsdf_vol.get_point_cloud_color()


    def get_mesh_for_instance(self, instanceNumber, score_threshold=0.1):
        if instanceNumber is None:
            return self.tsdf_vol.get_mesh(self.classes,_class=True, col_dict=self.color_dict, score_threshold = score_threshold)
        else:
            return self.tsdf_vol.get_mesh_for_instance(self.classes,class_number=instanceNumber, col_dict=self.color_dict, score_threshold = score_threshold)


