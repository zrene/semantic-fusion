import time
import cv2
import numpy as np

import preprocess_masks as pre
import os

import fusion

import numpy as np


# Which class should get assigned which color?
color_dict = {
    66: (255, 0, 0),
    15: (0, 255, 0),
    135: (0, 0, 255),
    2: (255, 0, 255),

    21: (0, 255, 255),
    36: (139,69,19),#desk,,
    19: (139,69,19),#desk
    119: (255, 255, 0),
    1: (255, 20,147) # book
}

# Which class should be extracted to point clouds
classes_to_extract = {
    'Computer': 66,
    'Paper': 15,
    'Towel': 135,
    'Bottle': 2,
    "Wall" : 21,
    "Pillow": 119,
    "Desk" :  36,
}

n_imgs = 2  # Not to much Img --> Death
inval = 1651  # To get a different View
pointcloud = False
mesh = True
write_pc = True
debug = False


path_to_mask_images = "mask_data"
path_to_rgbd = "data"

reproccess_images = False

if __name__ == "__main__":
    print("Estimating voxel volume bounds...")

    #viewer = pcl.pcl_visualization.PCLVisualizering()

    cam_intr = np.loadtxt("data/camera-intrinsics.txt", delimiter=' ')
    vol_bnds = np.zeros((3, 2))

    for i in range(n_imgs):
        i = i + inval
        # Read depth image and camera pose
        depth_im = cv2.imread(path_to_rgbd + "/%05d.png" % (i), -1).astype(float)
        depth_im /= 1000.  # depth is saved in 16-bit PNG in millimeters
        depth_im[depth_im == 65.535] = 0  # set invalid depth to 0 (specific to 7-scenes dataset)
        cam_pose = np.loadtxt(path_to_rgbd + "/%05d.txt" % (i))  # 4x4 rigid transformation matrix

        # Compute camera view frustum and extend convex hull
        view_frust_pts = fusion.get_view_frustum(depth_im, cam_intr, cam_pose)
        vol_bnds[:, 0] = np.minimum(vol_bnds[:, 0], np.amin(view_frust_pts, axis=1))
        vol_bnds[:, 1] = np.maximum(vol_bnds[:, 1], np.amax(view_frust_pts, axis=1))

        # Check if needed images are present. If not generate them
        if (reproccess_images or not (os.path.exists(path_to_mask_images + "/%05d" % (i) + "/color_image.jpg") and os.path.exists(path_to_mask_images + "/%05d" % (i) + "/class_image.png"))):
            print("color images or class image not found. Going to run preprocessing again")
            pre.preprocess_all(path_to_mask_images, path_to_rgbd)
            reproccess_images = False

    # ======================================================================================================== #



    # ======================================================================================================== #
    # Integrate
    # ======================================================================================================== #
    # Initialize voxel volume
    print("Initializing voxel volume...")
    tsdf_vol = fusion.TSDFVolume(vol_bnds, voxel_size=0.02)

    # Loop through RGB-D images and fuse them together
    t0_elapse = time.time()

    i = inval
    class_img = cv2.cvtColor(cv2.imread(path_to_mask_images + "/%05d/class_image.png" % (i)), cv2.COLOR_BGR2RGB)
    color_image = cv2.cvtColor(cv2.imread(path_to_mask_images + "/%05d/color_image.jpg" % (i)), cv2.COLOR_BGR2RGB)


    for i in range(n_imgs):
        i = i + inval
        print("Fusing frame (%d) %d/%d" % (i, i-inval, n_imgs))

        # Read RGB-D image and camera pose
        # color_image = cv2.cvtColor(cv2.imread("data/%05d.jpg"%(i)), cv2.COLOR_BGR2RGB)

        color_image = cv2.cvtColor(cv2.imread(path_to_mask_images + "/%05d/color_image.jpg" % (i)), cv2.COLOR_BGR2RGB)

        # Image containing class and score information RGB -> [0, class_number, score]
        class_img = cv2.cvtColor(cv2.imread(path_to_mask_images + "/%05d/class_image.png" % (i)), cv2.COLOR_BGR2RGB)




        depth_im = cv2.imread("data/%05d.png" % (i), -1).astype(float)
        depth_im /= 1000.
        depth_im[depth_im == 65.535] = 0
        cam_pose = np.loadtxt("data/%05d.txt" % (i))



        if(debug):
           cv2.imshow('image', np.vstack([color_image, class_img]))
           # cv2.imshow('image2', color_image)
           # Get a numpy array to display from the simulation
           cv2.waitKey(20)
           input("Press Enter to continue...")


        # Integrate observation into voxel volume (assume color aligned with depth)
        tsdf_vol.integrate(color_image, depth_im, class_img, cam_intr, cam_pose, obs_weight=1.)



    fps = n_imgs / (time.time() - t0_elapse)
    print("Average FPS: {:.2f}".format(fps))

    # Get mesh from voxel volume and save to disk (can be viewed with Meshlab)
    # print("Saving mesh to Office.ply...")
    # verts, faces, norms, colors = tsdf_vol.get_mesh()
    # fusion.meshwrite("Office.ply", verts, faces, norms, colors)

    if write_pc:
        # Get mesh from voxel volume and save to disk (can be viewed with Meshlab)
        print("Saving mesh to class ffice_class.ply")
        verts, faces, norms, colors = tsdf_vol.get_mesh(_class=True, col_dict=color_dict)
        fusion.meshwrite("output/Office_class.ply", verts, faces, norms, colors)

        if pointcloud:
            for class_ in classes_to_extract:
                try:
                    class_numb = classes_to_extract[class_]
                    print("Saving point cloud to output/" + class_+"_pc.ply .....")
                    point_cloud = tsdf_vol.get_point_cloud_for_instance(class_number=class_numb)
                    fusion.pcwrite("output/pc_" + class_+".ply", point_cloud)
                except ValueError:
                    print("Class not found")

        if mesh:
            for class_ in classes_to_extract:
                try:
                    class_numb = classes_to_extract[class_]
                    print("Saving mesh to " + "output/mesh_" + class_+"_pc.ply")
                    verts, faces, norms, colors = tsdf_vol.get_mesh_for_instance(class_number=class_numb, col_dict=color_dict)
                    fusion.meshwrite("output/mesh_" + class_+"_pc.ply", verts, faces, norms, colors)
                except ValueError:
                    print("Class not found")


        print("Saving all points to output/pc_all.ply...")
        point_cloud = tsdf_vol.get_point_cloud_for_instance(class_number=None)
        fusion.pcwrite("output/pc_all.ply", point_cloud)


        print("Saving all meshes to output/mesh_all.ply...")
        verts, faces, norms, colors = tsdf_vol.get_mesh_for_instance(class_number=None, col_dict=color_dict)
        fusion.meshwrite("output/mesh_all.ply", verts, faces, norms, colors)
