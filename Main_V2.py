import multiprocessing as mp
import multiprocessing as mp
import threading
import time
from queue import Empty as queue_empty

import numpy as np
import open3d as o3d
import pclpy
import preprocess_masks_v2 as pre
from pclpy import pcl
import fusion
import SemanticFuser
import pointcloud_utils as pc_utils
import csv

#basically flips a dictionary
def get_classes_to_extract(classes):
    dict = {}
    for key in classes.keys():
        dict[classes[key]] = int(key)
    return dict

#For legacy support, so we can still use the old nyu data
def nyu_classes(path= "labels_old.csv"):
    with open(path, newline='') as csvfile:
        labelreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        labels = {}
        for row in labelreader:
            labels[row[0]] = row[1]
    mapping = {
        'Unknown': 0,
        'Bed': 1,
        'Books': 2,
        'Ceiling': 3,
        "Chair": 4,
        "Floor": 5,
        "Furniture": 6,
        "Object": 7,
        "Picture": 8,
        "Sofa": 9,
        "Table": 10,
        "TV": 11,
        "Wall": 12,
        "Window": 13,
    }
    classes_to_extract = {
        'Computer': 66,
        'Paper': 15,
        'Towel': 135,
        'Bottle': 2,
        "Wall" : 21,
        "Pillow": 119,
        "Desk" :  36,
    }
    color_dict = {
        66: (255, 0, 0),
        15: (0, 0, 255),
        135: (255, 0, 0),
        2: (255, 255, 0),
        21: (255, 255, 255),
        119: (255, 0, 255),
        36: (0, 255, 255)
    }
    return labels, mapping, color_dict, classes_to_extract





######################
#####Parameters#######
######################
use_sopervoxel_postproc = True
n_images = 100
n_start = 0
reprocess_images = False
morph_preprocessing = False
visualize = False
labels_path = "labels_40.csv" #might be overwritten by the if statement in the main function
voxel_size = 0.04
filter_interval = -1 # do not apply median filter
distance = 0.1 #paramter to get mesh for instance: cluster radius
min_size = 100 #paramter to get mesh for instance: to avoid noise and small cluster/ num of vertics
#mask_path =  "mask_data"
#mask_path =  "Mask_data_v2"
#mask_path =  "Mask_data_40"
mask_path = r"ScanNet_Masks"
rgbd_path = "ScanNet_scene0000_00\color"
#rgbd_path = "data"

score_threshold = 0.0
supervoxel_params = [voxel_size, #seed resolution
                     1, #spatial importance
                     0.2, #normal importance
                     1.0, #color importance
                    ]
########################
if __name__ == '__main__':

    # will select the right classes to the given mask_path
    if mask_path == "Mask_data_40":
        labels_path = "labels_40.csv"
        classes, mapping, color_dict = pre.get_classes(labels_path)
        classes_to_extract = get_classes_to_extract(classes)
    elif mask_path== "Mask_data_v2":
        labels_path = "labels.csv"
        classes, mapping, color_dict = pre.get_classes(labels_path)
        classes_to_extract = get_classes_to_extract(classes)
    elif mask_path == "mask_data":
        labels_path = "labels_old.csv"
        classes, mapping, color_dict, classes_to_extract = nyu_classes(labels_path)
    else:
        classes, mapping, color_dict = pre.get_classes(labels_path)
        classes_to_extract = get_classes_to_extract(classes)
    print("----Class-info----")
    print("classes: " + str(classes))
    print("mapping: " + str(mapping))
    print("color_dict: " + str(color_dict))
    print("classes_to_extract: " + str(classes_to_extract))
    print("-------------------")



    fuser = SemanticFuser.SemanticFuser(classes, mapping, color_dict,
                                        n_imgs=n_images,
                                        start = n_start,
                                        path_to_rgbd= rgbd_path,
                                        path_to_mask_images = mask_path,
                                        reproccess_images=reprocess_images,
                                        score_threshold = score_threshold,
                                        supervoxel_params= supervoxel_params,
                                        morph_preprocessing = morph_preprocessing,
                                        voxel_size= voxel_size,
                                        filter_interval = filter_interval)


    for i in range(n_images-1):
        fuser.fuse_images(i)

    fuser.fuse_images(n_images)
    fuser.filter_volume()
    fuser.prepareVoxelExtraction()
    fuser.extractPc()
    fusion_seg, _ = fuser.svox.extract_point_cloud()


    if use_sopervoxel_postproc:
        fuser.prepareVoxelExtraction()
        fuser.svox.extract_point_cloud()
        fuser.createSuperVoxels()
        svox_seg_pc = fuser.svox.pc_segmented
        fuser.svox.extract_volume()

        if visualize:
            viewer = pcl.visualization.PCLVisualizer("(using Supervoxel) Viewer")
            viewer.setPosition(0, 0)
            viewer.setSize(800, 400)
            viewer.addPointCloud(svox_seg_pc)

    if visualize:
        segmentationViewer = pcl.visualization.PCLVisualizer("(no supervoxel) Viewer")
        segmentationViewer.setPosition(0, 500)
        segmentationViewer.setSize(800, 400)
        segmentationViewer.addPointCloud(fusion_seg)


    print("Saving all meshes to output/mesh_all.ply")
    verts, faces, norms, colors, original_colors, _ = fuser.get_mesh_for_instance(None, score_threshold=0)
    fusion.meshwrite("output/mesh_all.ply", verts, faces, norms, colors)

    fusion.pcwrite("output/pc_all.ply",  np.hstack([verts, colors]))
    # use original color
    # fusion.meshwrite("output/mesh_all.ply", verts, faces, norms, original_colors)

    if use_sopervoxel_postproc:
        print("Saving all meshes to output/mesh_svox_all.ply")
        pc, norms = fuser.svox.get_pc_for_id(None)
        pc_utils.storeTriangleMesh(pc, norms, "output/mesh_svox_all.ply")

        print("Saving point cloud to output/pc_svox_all.ply .....")
        fusion.pcwrite("output/pc_svox_all.ply", pc)

    for (label, numb) in classes_to_extract.items():
        try:
            print("Saving instance", label, "to output/mesh_"+str(label)+".ply")
            verts, faces, norms, colors, original_colors, _ = fuser.get_mesh_for_instance(numb, score_threshold=0.1)
            fusion.meshwrite("output/mesh_"+str(label)+".ply", verts, faces, norms, original_colors)
            fusion.get_mesh_for_subinstance(verts,faces,norms,colors,label,distance,min_size)

            print("Saving instance", label, "to output/pc_all.ply")
            fusion.pcwrite("output/pc " + label + "all.ply", np.hstack([verts, original_colors]))

            # use original color
            # fusion.meshwrite("output/mesh_"+str(label)+".ply", verts, faces, norms, original_colors)

            if use_sopervoxel_postproc:
                pc, norms = fuser.svox.get_pc_for_id(numb)
                pc_utils.storeTriangleMesh(pc, norms, "output/mesh_svox_"+str(label)+".ply")

        except ValueError as e:
            print("got error ", e)
            print("Assuming class not in volume")

        try:
            print("Saving instance", label, "to output/pc_" + str(label) + ".ply")
            point_cloud = fuser.tsdf_vol.get_point_cloud_for_instance(class_number=numb)
            fusion.pcwrite("output/pc_" + label + ".ply", point_cloud)

            if use_sopervoxel_postproc:
                pc, _ = fuser.svox.get_pc_for_id(numb)
                print("Saving point cloud to output/pc_svox_" + label + ".ply .....")
                fusion.pcwrite("output/pc_svox_" + label + ".ply", pc)

        except ValueError as e:
            print("got error ", e)
            print("Assuming class not in volume")





