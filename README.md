# 3D Instance Fusion

### preprocess_masks.py
Converts each mask from the Mask R-CNN into a class image. 

A class image contains the class number as g value and the matching score as b value for each pixel

### main.py
Run this to create the 3d mesh.

### output/office.mlp
Open this with [MeshLab](http://www.meshlab.net/]), to see the output.